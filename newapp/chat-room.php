<?php session_start();?>
<html>
    <head>
        <title>Chat Room</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
          //Create connection with the database (creditentials)
            $username = "";
            $db_host = "master";
            $db_name = "web_assignment";
            $db_user = "root";
            $db_pass  = "pass123";

            // Create mysqli object with connection
            $db = new mysqli($db_host, $db_user, $db_pass, $db_name);
            if ($db -> connect_error){
                printf("Connection failure : %s\n, $db -> connect_error");
                exit();
            } 

            $id = $_SESSION["id"];
            $query = "SELECT * FROM users WHERE id <> $id";
            $result = mysqli_query($db, $query) or die(mysqli_error($db));
            if (mysqli_num_rows($result) >= 0){
                $friendsInfo = array();
                $friendOnlineStatus = array();
                foreach($result as $row){
                    $id = $row["id"];
                    $username = $row["username"];

                    $new = array(
                        'id' => $id,
                        'username' => $username
                    );

                    array_push($friendsInfo, $new);
            }

                $query1 = "SELECT * FROM login_details WHERE userid = $id";
                $result2 = mysqli_query($db, $query1) or die(mysqli_error($db));
                if (mysqli_num_rows($result2) > 0){
                    $data = array(
                            'username' => $username,
                            'id' => $id,
                            'status' => 'True'
                        );
                        array_push($friendOnlineStatus, $data);
                } else{
                    $data = array(
                        'username' => $username,
                        'id' => $id,
                        'status' => 'False'
                    );
                    array_push($friendOnlineStatus, $data);
                }
            }
        ?>
        <h3 align = "center">Chat Room</h3>
        <table class = "table table-bordered table-striped">
            <tr>
                <th>Username</th>
                <th>Status</th>
                <th>Chat</th>
            </tr>
                <?php foreach($friendOnlineStatus as $value):?>
                    <tr>
                        <td>
                            <?php echo $value['username'];?>
                        </td>
                        <td>
                            <?php if ($value['status'] == 'True'):?>
                                <span class = "label label-success">Online</span>
                            <?php else :?>
                                <span class = "label label-danger">Offline</span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <form action = "chatmessages.php" method = "get">
                                <input type = "hidden" name = "id" value = "<?php echo $value['id'];?>">
                                <input type = "hidden" name = "username" value = "<?php echo $value['username'];?>">
                                <button type = "submit" class = "btn btn-info btn-xs start_chat" name = "messages" value = "1">Start Chat</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tr>
        </table>
    </body>
</html>



            