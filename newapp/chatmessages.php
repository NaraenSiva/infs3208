<?php session_start();?>
<html>
    <head>
        <title>Chat Box</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel = "stylesheet" type = "text/css" href = "css/style7.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class = "user_dialog" id = "<?php echo $_GET["id"];?>">
        <div id = "title">
            <h2>Commenting with <?php echo $_GET["username"];?></h2>
        </div>
        <div class = "chat_history" id = "history">
            <?php for($x = 0; $x < sizeof($_SESSION["data"]); $x++):?>
                <p id = "name">
                    <?php echo $_SESSION["username"];?>
                </p>
                <p id = "message">
                    <?php echo $_SESSION["data"][$x]['messages'];?>
                    <em><?php echo $_SESSION["data"][$x]['timestampsd'];?></em>
                </p>
            <?php endfor;?>
        </div>
        <form action = "messaging.php" method = "post">
            <div class = "form-group">
                <textarea name = "chat_message" id = "message" class = "form-control"></textarea>
            </div>
            <input type = "hidden" name = "from_id" id = "from_id" value = "<?php echo $_SESSION["id"];?>">
            <div class = "form_group">
                <button type = "submit" name = "send_chat" id = "chat_send" class = "btn btn-info send_chat">Send</button>
            </div>
        </form>
    </div>
    </body>
    <script>
         $(document).ready(function(){
            setInterval(() => {
                update_chat_history();
            }, 1500);
        });

        function update_chat_history(){
            var from_user = $('#from_id').val();
            $.ajax({
                type: "post",
                url: "messages.php",
                data: {
                    type: 1,
                    from_user: from_user
                },
                cache: false,
                success: function(data){
                }   
            });
        };

        $(document).ready(function() {
            $('#chat_send').on('click', function() {
                var message = $('textarea#message').val();
                var fromid = $('#from_id').val();
                $("#chat_send").attr("disabled", "disabled");
                $.ajax({
                    url: "messages.php",
                    type: "POST",
                    data: {
                        type: 2,
                        message: message,
                        fromid: fromid
                    },
                    cache: false,
                    success: function(dataResult){
                        var dataResult = JSON.parse(dataResult);
                        if(dataResult.statusCode==200){
                            update_chat_history();
                            $("#chat_send").removeAttr("disabled");
                            $("textarea#message").val('').change();
                        }
                    }
                });
            });
        });
    </script>
</html>