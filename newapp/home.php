<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
        <link rel ="stylesheet" href = "css/style.css">
    </head>
    <body>
        <form action ="authenticate.php" method="post" enctype='multipart/form-data'>
            <div class = "register-form">
                <p>You seem to have incorrectly put in information or your account does not exist</p>
                <p>Please try again by clicking this button</p>
                <center><a href = "index.php">Return to Login Page</a><center>
            </div>
        </form>   
    </body>
</html>