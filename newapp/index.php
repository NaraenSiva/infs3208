<?php session_start();?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Login</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
        <link rel ="stylesheet" href = "css/style.css">
	</head>
    <body>
        <form action = "authenticate.php" method="post">
            <div class="login-form">
                <div class = "title">
                    <h2>Welcome to CheckingYourProfile!</h2>
                </div>
                <div id = "message">
                <?php if (isset($name_error)): ?>
                    <span><?php echo $name_error; ?></span>
                <?php endif ?>
                </div>
                <label for="username"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" value = "<?php if (isset($_COOKIE["userName"])){ echo $_COOKIE["userName"];}?>" required>

                <label for="password"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" value = "<?php if (isset($_COOKIE["userPassword"])){ echo $_COOKIE["userPassword"];}?>" required>

                <div id = "remember">
                    <label for="remember-me"><b>Remember Me</b></label>
                    <input type ="checkbox" name = "remember" <?php if(isset($_COOKIE["userName"])){ ?> checked <?php } ?>/>  
                </div>
                
                <button name = "login" type="submit">Login</button>
            </div>

            <div class="helper" style="background-color:#f1f1f1">
                <span class = "register">Haven't Registered Yet? <a href = "register.php">Click Here</a></span>
            </div>
        </form>
    </body>
</html>