<?php session_start();?>
<!DOCTYPE html>
<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel ="stylesheet" href = "css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    </head>
    <body>
        <form action ="authenticate.php" method="post" enctype='multipart/form-data'>
            <div class = "register-form">
                <div class = "title">
                    <h2>Welcome to CheckingYourProfile!</h2>
                    <h4>Please Enter Your Details</h4>
                </div>
                <input type="text" placeholder="Enter Username" name="username">
                <input type="text" placeholder="Enter your Email" name="email">
                <input type="text" placeholder="Enter Password" name="password">
                <input type="text" placeholder="Please Re-enter your Password" name="repassword">
                <button name = "register" type="submit">Register</button>
            </div>
        </form>   
    </body>
</html>