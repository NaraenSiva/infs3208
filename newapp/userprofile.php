<?php session_start();?>
<!DOCTYPE html>
<html>
    <head>
        <title>UserProfile</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel = "stylesheet" href = "css/style3.css">
    </head>
    <body>
        <div id = "details">
            <div id = "top" align="center" class = "mx-auto">
                <h3>User Details: <?php echo $_SESSION["username"];?></h3>
                <div id = "nav">
                    <h4><a href = "chat-room.php">Chat Room</a></h4>
                    <h4><a href = "process.php">Logout</a></h4>
                </div>
            </div>
            <?php
            $username = "";
            $db_host = "master";
            $db_name = "web_assignment";
            $db_user = "root";
            $db_pass  = "pass123";
            
            // Create mysqli object with connection
            $db = new mysqli($db_host, $db_user, $db_pass, $db_name);
            if ($db -> connect_error){
                printf("Connection failure : %s\n, $db -> connect_error");
                exit();
            } 

            $selectusername = $_SESSION["username"];
            $sqlquery = "SELECT * FROM users WHERE username='$selectusername'";
            $result = mysqli_query($db, $sqlquery) or die(mysqli_error($db));

            if (mysqli_num_rows($result) > 0){
                $name_error = "Not possible";
            } else{
                $_SESSION["value"] = $result;
            }
            ?>
            <table class = "table table-hover">
                <?php foreach($result as $row):?>
                    <tr>
                        <td>Username</td>
                        <td><?php echo $row["username"];?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><?php echo $row["email"];?></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><?php echo $row["dbpassword"];?></td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
        <div class="container">
            <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	        </div>
            <div class="form-group">
                <h2 align="center">Update Username</h2>
                <label for="email">Username:</label>
                <input type="text" class="form-control" id="username" placeholder="Enter New Username" name="username">
                <button type="submit" class="btn btn-primary" id="usernameupdate">Submit</button>
            </div>
            <div class="form-group">
            <h2 align="center">Update Email</h2>
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email">
                <button type="submit" class="btn btn-primary" id="emailupdate">Submit</button>
            </div>
            <div class="form-group">
                <h2 align="center">Update Password</h2>
                <label for="email">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter New Password" name="password">
            </div>
            <div class="form-group">
                <label for="email">Repeat Password Change:</label>
                <input type="password" class="form-control" id="repassword" placeholder="Re-enter Password" name="repassword">
                <button type="submit" class="btn btn-primary" id="passwordupdate">Submit</button>
            </div>
        </div>
        <script>
        $(document).ready(function() {
            $('#usernameupdate').on('click', function() {
                var username = $('#username').val();
                if(username!=""){
                    $("#usernameupdate").attr("disabled", "disabled");
                    $.ajax({
                        url: "checking.php",
                        type: "POST",
                        data: {
                            type: 1,
                            username: username
                        },
                        cache: false,
                        success: function(dataResult){
                            var dataResult = JSON.parse(dataResult);
                            if(dataResult.statusCode==200){
                                $("#butsave").removeAttr("disabled");
                                $('#fupForm').find('input:text').val('');
                                $("#success").show();
                                $('#success').html('Username Updated! Refresh your browser to see the changes'); 						
                            }
                            else if(dataResult.statusCode==201){
                            alert("Error occured, someone has this username!");
                            }
                            
                        }
                    });
                }
                else{
                    alert('Please fill all the field !');
                }
            });
        });
        </script>
        <script>
        $(document).ready(function() {
            $('#emailupdate').on('click', function() {
                var email = $('#email').val();
                if(email!=""){
                    $("#usernameupdate").attr("disabled", "disabled");
                    $.ajax({
                        url: "checking.php",
                        type: "POST",
                        data: {
                            type: 2,
                            email: email
                        },
                        cache: false,
                        success: function(dataResult){
                            var dataResult = JSON.parse(dataResult);
                            if(dataResult.statusCode==200){
                                $("#butsave").removeAttr("disabled");
                                $('#fupForm').find('input:text').val('');
                                $("#success").show();
                                $('#success').html('Email Updated! Refresh your browser to see the changes'); 						
                            }
                            else if(dataResult.statusCode==201){
                            alert("Error occured !");
                            }    
                        }
                    });
                }
                else{
                    alert('Please fill all the field !');
                }
            });
        });
    </script>
     <script>
        $(document).ready(function() {
            $('#passwordupdate').on('click', function() {
                var password = $('#password').val();
                var repassword = $('#repassword').val();
                if(password != " " && repassword != " " && (password == repassword)){
                    $("#passwordupdate").attr("disabled", "disabled");
                    $.ajax({
                        url: "checking.php",
                        type: "POST",
                        data: {
                            type: 3,
                            password: password
                        },
                        cache: false,
                        success: function(dataResult){
                            var dataResult = JSON.parse(dataResult);
                            if(dataResult.statusCode==200){
                                $("#butsave").removeAttr("disabled");
                                $('#fupForm').find('input:text').val('');
                                $("#success").show();
                                $('#success').html('Password Updated! Refresh your browser to see the changes'); 						
                            }
                            else if(dataResult.statusCode==201){
                            alert("Error occured !");
                            }    
                        }
                    });
                }
                else{
                    alert('Please fill all the field !');
                }
            });
        });
    </script>
    </body>          
<html>